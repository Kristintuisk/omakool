﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(OmaKool.Startup))]
namespace OmaKool
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
